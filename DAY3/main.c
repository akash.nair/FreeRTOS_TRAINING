/*
 * main.c
 *
 *  Created on: 20-Jun-2018
 *      Author: akash
 */
// Standard includes
#include <stdio.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

// Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "interrupt.h"
#include "hw_apps_rcm.h"
#include "prcm.h"
#include "rom.h"
#include "rom_map.h"
#include "prcm.h"
#include "gpio.h"
#include "utils.h"
#include "interrupt.h"
#include "pin.h"

// Common interface includes
#include "gpio_if.h"
#include "button_if.h"
#include "pinmux.h"


P_INT_HANDLER g_S2InterruptHdl;
P_INT_HANDLER g_S3InterruptHdl;

//*****************************************************************************
//                      MACRO DEFINITIONS
//*****************************************************************************
#define APPLICATION_VERSION     "1.1.1"
#define STACK_SIZE              512

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
SemaphoreHandle_t sema_handle = NULL;
TaskHandle_t taskhandler1;
TaskHandle_t taskhandler2;

#ifndef USE_TIRTOS
/* in case of TI-RTOS don't include startup_*.c in app project */
#if defined(gcc) || defined(ccs)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif
#endif
//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************


//*****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//*****************************************************************************
static void vTask1( void *p);
static void vTask2( void *p);
static void S2InterruptHdl( void *p);
static void S3InterruptHdl( void *p);
static void BoardInit();

//*****************************************************************************
//                      LOCAL FUNCTION DEFINITIONS
//*****************************************************************************
#ifdef USE_FREERTOS
//*****************************************************************************
// FreeRTOS User Hook Functions enabled in FreeRTOSConfig.h
//*****************************************************************************

//*****************************************************************************
//
//! \brief Application defined hook (or callback) function - assert
//!
//! \param[in]  pcFile - Pointer to the File Name
//! \param[in]  ulLine - Line Number
//!
//! \return none
//!
//*****************************************************************************
void vAssertCalled( const char *pcFile, unsigned long ulLine )
{
}

//*****************************************************************************
//
//! \brief Application defined idle task hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationIdleHook( void)
{
}

//*****************************************************************************
//
//! \brief Application defined malloc failed hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationMallocFailedHook()
{
}

//*****************************************************************************
//
//! \brief Application defined stack overflow hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationStackOverflowHook(void)
{

}

#endif //USE_FREERTOS



/*****************************
     2 TASk OF XTASKCREATE()
 *****************************/

void vTask1( void *p)
{
    while(1) {
        xSemaphoreTake(sema_handle,10000);
        GPIO_IF_LedOff(MCU_ALL_LED_IND);
        vTaskDelay(50);
        GPIO_IF_LedOn(MCU_ALL_LED_IND);
        vTaskDelay(5000);
        GPIO_IF_LedOff(MCU_ALL_LED_IND);
        xSemaphoreGive(sema_handle);
        Button_IF_EnableInterrupt(SW3);
        vTaskSuspend( NULL);
    }
}

void vTask2( void *p)
{
    while(1) {
        xSemaphoreTake(sema_handle,10000);
        uint32_t j = 6;
        GPIO_IF_LedOff(MCU_ALL_LED_IND);
        while(j)
        {
            vTaskDelay(500);
            GPIO_IF_LedOn(MCU_ALL_LED_IND);
            vTaskDelay(500);
            GPIO_IF_LedOff(MCU_ALL_LED_IND);
            j--;
        }
        xSemaphoreGive(sema_handle);
        Button_IF_EnableInterrupt(SW2);
        vTaskSuspend( NULL);
   }
}

void S2InterruptHdl( void *p)
{
    Button_IF_DisableInterrupt(SW3);
    vTaskResume(taskhandler1);
    Button_IF_EnableInterrupt(SW2);
}
void S3InterruptHdl( void *p)
{
    Button_IF_DisableInterrupt(SW2);
    vTaskResume(taskhandler2);
    Button_IF_EnableInterrupt(SW3);
}
//*****************************************************************************
//
//! Board Initialization & Configuration
//!
//! \param  None
//!
//! \return None
//
//*****************************************************************************
static void
BoardInit(void)
{
/* In case of TI-RTOS vector table is initialize by OS itself */
#ifndef USE_TIRTOS
    //
    // Set vector table base
    //
#if defined(ccs) || defined(gcc)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif

    //
    // Enable Processor
    //
    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}

int
main()
{

    BoardInit();
    PinMuxConfig();

    GPIO_IF_LedConfigure(LED1|LED2|LED3);
    GPIO_IF_LedOff(MCU_ALL_LED_IND);
    MAP_UtilsDelay(8000000);
    GPIO_IF_LedOn(MCU_ALL_LED_IND);
    MAP_UtilsDelay(8000000);
    GPIO_IF_LedOff(MCU_ALL_LED_IND);

    sema_handle = xSemaphoreCreateMutex();

    xTaskCreate(vTask1, "LED_SW1", (STACK_SIZE / (sizeof(uint32_t))), NULL, 1,&taskhandler1);
    vTaskSuspend(taskhandler1);
    xTaskCreate(vTask2, "LED_SW2", (STACK_SIZE / (sizeof(uint32_t))), NULL, 1,&taskhandler2);
    vTaskSuspend(taskhandler2);

    Button_IF_Init(S2InterruptHdl, S3InterruptHdl);

    vTaskStartScheduler();
    return 0;
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
