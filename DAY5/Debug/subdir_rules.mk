################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
gpio_if.obj: /home/nirmal/ti/sdks/cc3200-sdk/example/common/gpio_if.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=none -me --include_path="/home/nirmal/workspace_v8/Day_5_Led_blink_FreeRTOS_memory" --include_path="/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/driverlib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/oslib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/Source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/inc" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/example/common" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --define=ccs --define=NON_NETWORK --define=USE_FREERTOS --define=cc3200 -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="gpio_if.d_raw" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '

main.obj: ../main.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=none -me --include_path="/home/nirmal/workspace_v8/Day_5_Led_blink_FreeRTOS_memory" --include_path="/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/driverlib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/oslib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/Source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/inc" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/example/common" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --define=ccs --define=NON_NETWORK --define=USE_FREERTOS --define=cc3200 -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="main.d_raw" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '

pinmux.obj: ../pinmux.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=none -me --include_path="/home/nirmal/workspace_v8/Day_5_Led_blink_FreeRTOS_memory" --include_path="/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/driverlib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/oslib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/Source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/inc" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/example/common" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --define=ccs --define=NON_NETWORK --define=USE_FREERTOS --define=cc3200 -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="pinmux.d_raw" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '

port.obj: /home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3/port.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=none -me --include_path="/home/nirmal/workspace_v8/Day_5_Led_blink_FreeRTOS_memory" --include_path="/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/driverlib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/oslib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/Source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/inc" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/example/common" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --define=ccs --define=NON_NETWORK --define=USE_FREERTOS --define=cc3200 -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="port.d_raw" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '

startup_ccs.obj: /home/nirmal/ti/sdks/cc3200-sdk/example/common/startup_ccs.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=none -me --include_path="/home/nirmal/workspace_v8/Day_5_Led_blink_FreeRTOS_memory" --include_path="/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/driverlib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/oslib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/Source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/inc" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/example/common" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --define=ccs --define=NON_NETWORK --define=USE_FREERTOS --define=cc3200 -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="startup_ccs.d_raw" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '

tasks.obj: /home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/tasks.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=none -me --include_path="/home/nirmal/workspace_v8/Day_5_Led_blink_FreeRTOS_memory" --include_path="/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/driverlib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/oslib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/Source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/inc" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/example/common" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --define=ccs --define=NON_NETWORK --define=USE_FREERTOS --define=cc3200 -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="tasks.d_raw" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '

uart_if.obj: /home/nirmal/ti/sdks/cc3200-sdk/example/common/uart_if.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: "$<"'
	@echo 'Invoking: ARM Compiler'
	"/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=none -me --include_path="/home/nirmal/workspace_v8/Day_5_Led_blink_FreeRTOS_memory" --include_path="/home/nirmal/ti/ccsv8/tools/compiler/ti-cgt-arm_18.1.2.LTS/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/driverlib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/oslib" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/Source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/simplelink/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/inc" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/example/common" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/nirmal/ti/sdks/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --define=ccs --define=NON_NETWORK --define=USE_FREERTOS --define=cc3200 -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="uart_if.d_raw" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: "$<"'
	@echo ' '


