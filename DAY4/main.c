/*
 * main.c
 *
 *  Created on: 20-Jun-2018
 *      Author: akash
 */
// Standard includes
#include <stdio.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

// Driverlib includes
#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "interrupt.h"
#include "hw_apps_rcm.h"
#include "prcm.h"
#include "rom.h"
#include "rom_map.h"
#include "prcm.h"
#include "gpio.h"
#include "utils.h"
#include "interrupt.h"
#include "pin.h"
#include "timers.h"

// Common interface includes
#include "gpio_if.h"
#include "button_if.h"
#include "pinmux.h"


//*****************************************************************************
//                      MACRO DEFINITIONS
//*****************************************************************************
#define APPLICATION_VERSION     "1.1.1"
#define STACK_SIZE              2048
#define NUM_TIMERS 5

//*****************************************************************************
//                 GLOBAL VARIABLES -- Start
//*****************************************************************************
long x;


#ifndef USE_TIRTOS
/* in case of TI-RTOS don't include startup_*.c in app project */
#if defined(gcc) || defined(ccs)
extern void (* const g_pfnVectors[])(void);
#endif
#if defined(ewarm)
extern uVectorEntry __vector_table;
#endif
#endif
//*****************************************************************************
//                 GLOBAL VARIABLES -- End
//*****************************************************************************


//*****************************************************************************
//                      LOCAL FUNCTION PROTOTYPES
//*****************************************************************************
static void vTask1( void *p);
static void vTask2( void *p);
static void BoardInit();

//*****************************************************************************
//                      LOCAL FUNCTION DEFINITIONS
//*****************************************************************************
#ifdef USE_FREERTOS
//*****************************************************************************
// FreeRTOS User Hook Functions enabled in FreeRTOSConfig.h
//*****************************************************************************

//*****************************************************************************
//
//! \brief Application defined hook (or callback) function - assert
//!
//! \param[in]  pcFile - Pointer to the File Name
//! \param[in]  ulLine - Line Number
//!
//! \return none
//!
//*****************************************************************************
void vAssertCalled( const char *pcFile, unsigned long ulLine )
{
}

//*****************************************************************************
//
//! \brief Application defined idle task hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationIdleHook( void)
{
}

//*****************************************************************************
//
//! \brief Application defined malloc failed hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************
void vApplicationMallocFailedHook()
{
}

//*****************************************************************************
//
//! \brief Application defined stack overflow hook
//!
//! \param  none
//!
//! \return none
//!
//*****************************************************************************


static void
BoardInit(void)
{
    /* In case of TI-RTOS vector table is initialize by OS itself */
#ifndef USE_TIRTOS
    //
    // Set vector table base
    //
#if defined(ccs) || defined(gcc)
    MAP_IntVTableBaseSet((unsigned long)&g_pfnVectors[0]);
#endif
#if defined(ewarm)
    MAP_IntVTableBaseSet((unsigned long)&__vector_table);
#endif
#endif

    //
    // Enable Processor
    //
    MAP_IntMasterEnable();
    MAP_IntEnable(FAULT_SYSTICK);

    PRCMCC3200MCUInit();
}

void vApplicationStackOverflowHook(void)
{
}

#endif //USE_FREERTOS

//void vTimerCallback(TimerHandle_t xTimer);


void vTask1( void *p)
{

    uint32_t j,q,w;
    // GPIO_IF_LedOff(MCU_ALL_LED_IND);
    // while(1) {
    //    xTimerStart( xTimers[ x ], 0 );
    while(1) {
        //for (w = 0; w < 6; w++) {
        for (j = 0; j < 2500000; j++);
        //vTaskDelay(5000);
        GPIO_IF_LedOn(MCU_GREEN_LED_GPIO);
        for (q = 0; q < 2500000; q++);
        //vTaskDelay(5000);
        GPIO_IF_LedOff(MCU_GREEN_LED_GPIO);
        // j--;
        // }
    }
    vTaskDelete(NULL);
}

void vTask2( void *p)
{
    // while(1) {

    uint32_t i,k, z;
    //        for (z = 0; z < 6; z++ ) {
    while(1) {
        for (i = 0; i < 2500000; i++);
        //vTaskDelay(5000);
        GPIO_IF_LedOn(MCU_ORANGE_LED_GPIO);
        for (k = 0; k < 2500000; k++);
        //vTaskDelay(5000);
        GPIO_IF_LedOff(MCU_ORANGE_LED_GPIO);
    }
    vTaskDelete(NULL);
    //}
}

/*
void vTask3( void *p)
{
    // while(1) {

    uint32_t o,n, c;
    //        for (o = 0; o < 6; o++ ) {
    while(1) {
        for (n = 0; n < 2500000; n++);
        //vTaskDelay(5000);
c        for (c = 0; c < 2500000; c++);
        //vTaskDelay(5000);
        GPIO_IF_LedOff(MCU_RED_LED_GPIO);
    }
    vTaskDelete(NULL);
    //}
}

 */


/* An array to hold handles to the created timers. */
xTimerHandle xTimers[ NUM_TIMERS ];

/* An array to hold a count of the number of times each timer expires. */
long lExpireCounters[ NUM_TIMERS ] = { 0 };

/* Define a callback function that will be used by multiple timer instances.
The callback function does nothing but count the number of times the
associated timer expires, and stop the timer once the timer has expired
10 times. */
void vTimerCallback( xTIMER *pxTimer )
{
    long lArrayIndex;
    const long xMaxExpiryCountBeforeStopping = 10;

    /* Optionally do something if the pxTimer parameter is NULL. */
    configASSERT( pxTimer );

    /* Which timer expired? */
    lArrayIndex = ( long ) pvTimerGetTimerID( pxTimer );

    /* Increment the number of times that pxTimer has expired. */
    lExpireCounters[ lArrayIndex ] += 1;

    /* If the timer has expired 10 times then stop it from running. */
    if( lExpireCounters[ lArrayIndex ] == xMaxExpiryCountBeforeStopping )
    {
        /* Do not use a block time if calling a timer API function from a
        timer callback function, as doing so could cause a deadlock! */
        xTimerStop( pxTimer, 0 );
    }
}


void vTask3(void *p)
{
    xTimerStart(xTimers[x], 0)
            GPIO_IF_LedOn(MCU_RED_LED_GPIO);
    vTaskDelay(2500);
    GPIO_IF_LedOff(MCU_RED_LED_GPIO);
}


int
main()
{
    long x;
    for (x = 0; x < 5; x++) {
        xTimerCreate("My_timer", (100 * x), pdTRUE,(void *)x, vTimerCallback);
        if( xTimers[ x ] == NULL )
        {
            /* The timer was not created. */
        }
        else
        {
            /* Start the timer.  No block time is specified, and even if one was
            it would be ignored because the scheduler has not yet been
            started. */
            if( xTimerStart( xTimers[ x ], 0 ) != pdPASS )
            {
                /* The timer could not be set into the Active state. */
            }
        }
    }

}

BoardInit();
PinMuxConfig();

GPIO_IF_LedConfigure(LED1|LED2|LED3);

GPIO_IF_LedOff(MCU_ALL_LED_IND);
MAP_UtilsDelay(8000000);
GPIO_IF_LedOn(MCU_ALL_LED_IND);
MAP_UtilsDelay(8000000);
GPIO_IF_LedOff(MCU_ALL_LED_IND);

// unsigned long ulTimer = 1UL;


xTaskCreate(vTask1, "LED_SW1", (STACK_SIZE / (sizeof(uint32_t))), NULL, 1, NULL);
xTaskCreate(vTask2, "LED_SW2", (STACK_SIZE / (sizeof(uint32_t))), NULL, 1, NULL);
xTaskCreate(vTask3, "LED_SW3", (STACK_SIZE / (sizeof(uint32_t))), NULL, 1, NULL);


vTaskStartScheduler();
return 0;
}
