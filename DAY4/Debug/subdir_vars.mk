################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../cc3200v1p32.cmd 

C_SRCS += \
../gpio_if.c \
../main.c \
../pinmux.c \
../startup_ccs.c \
../timers.c 

C_DEPS += \
./gpio_if.d \
./main.d \
./pinmux.d \
./startup_ccs.d \
./timers.d 

OBJS += \
./gpio_if.obj \
./main.obj \
./pinmux.obj \
./startup_ccs.obj \
./timers.obj 

OBJS__QUOTED += \
"gpio_if.obj" \
"main.obj" \
"pinmux.obj" \
"startup_ccs.obj" \
"timers.obj" 

C_DEPS__QUOTED += \
"gpio_if.d" \
"main.d" \
"pinmux.d" \
"startup_ccs.d" \
"timers.d" 

C_SRCS__QUOTED += \
"../gpio_if.c" \
"../main.c" \
"../pinmux.c" \
"../startup_ccs.c" \
"../timers.c" 


