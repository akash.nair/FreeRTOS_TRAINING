################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
gpio_if.obj: ../gpio_if.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib -me --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/MemMang" --include_path="/home/akash/Rtos/cc3200-sdk/example/interrupt" --include_path="/home/akash/workspace_v7/space/Day4" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/akash/Rtos/cc3200-sdk/oslib" --include_path="/home/akash/Rtos/cc3200-sdk/driverlib" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/source" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/include" --include_path="/home/akash/Rtos/cc3200-sdk/inc" --include_path="/home/akash/Rtos/cc3200-sdk/example/common" --include_path="/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --define=ccs --define=cc3200 --define=USE_FREERTOS --define=NON_NETWORK -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="gpio_if.d" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: $<'
	@echo ' '

main.obj: ../main.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib -me --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/MemMang" --include_path="/home/akash/Rtos/cc3200-sdk/example/interrupt" --include_path="/home/akash/workspace_v7/space/Day4" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/akash/Rtos/cc3200-sdk/oslib" --include_path="/home/akash/Rtos/cc3200-sdk/driverlib" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/source" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/include" --include_path="/home/akash/Rtos/cc3200-sdk/inc" --include_path="/home/akash/Rtos/cc3200-sdk/example/common" --include_path="/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --define=ccs --define=cc3200 --define=USE_FREERTOS --define=NON_NETWORK -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="main.d" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: $<'
	@echo ' '

pinmux.obj: ../pinmux.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib -me --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/MemMang" --include_path="/home/akash/Rtos/cc3200-sdk/example/interrupt" --include_path="/home/akash/workspace_v7/space/Day4" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/akash/Rtos/cc3200-sdk/oslib" --include_path="/home/akash/Rtos/cc3200-sdk/driverlib" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/source" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/include" --include_path="/home/akash/Rtos/cc3200-sdk/inc" --include_path="/home/akash/Rtos/cc3200-sdk/example/common" --include_path="/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --define=ccs --define=cc3200 --define=USE_FREERTOS --define=NON_NETWORK -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="pinmux.d" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: $<'
	@echo ' '

startup_ccs.obj: ../startup_ccs.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib -me --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/MemMang" --include_path="/home/akash/Rtos/cc3200-sdk/example/interrupt" --include_path="/home/akash/workspace_v7/space/Day4" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/akash/Rtos/cc3200-sdk/oslib" --include_path="/home/akash/Rtos/cc3200-sdk/driverlib" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/source" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/include" --include_path="/home/akash/Rtos/cc3200-sdk/inc" --include_path="/home/akash/Rtos/cc3200-sdk/example/common" --include_path="/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --define=ccs --define=cc3200 --define=USE_FREERTOS --define=NON_NETWORK -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="startup_ccs.d" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: $<'
	@echo ' '

timers.obj: ../timers.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Compiler'
	"/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/bin/armcl" -mv7M4 --code_state=16 --float_support=vfplib -me --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/MemMang" --include_path="/home/akash/Rtos/cc3200-sdk/example/interrupt" --include_path="/home/akash/workspace_v7/space/Day4" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/portable/CCS/ARM_CM3" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source/include" --include_path="/home/akash/Rtos/cc3200-sdk/third_party/FreeRTOS/source" --include_path="/home/akash/Rtos/cc3200-sdk/oslib" --include_path="/home/akash/Rtos/cc3200-sdk/driverlib" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/source" --include_path="/home/akash/Rtos/cc3200-sdk/simplelink/include" --include_path="/home/akash/Rtos/cc3200-sdk/inc" --include_path="/home/akash/Rtos/cc3200-sdk/example/common" --include_path="/home/akash/ti/ccsv7/tools/compiler/ti-cgt-arm_16.9.3.LTS/include" --define=ccs --define=cc3200 --define=USE_FREERTOS --define=NON_NETWORK -g --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --preproc_with_compile --preproc_dependency="timers.d" $(GEN_OPTS__FLAG) "$(shell echo $<)"
	@echo 'Finished building: $<'
	@echo ' '


